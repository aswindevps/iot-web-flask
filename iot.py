from flask import Flask
from flask import jsonify
from flask import request
from flask_cors import CORS
from connector.postgres import PostgresDB
import json
import datetime

postgresInstance =''

app = Flask(__name__)
CORS(app)

def connectDB():
    global postgresInstance
    postgresInstance = PostgresDB()
    postgresInstance.connect()

def myconverter(o):
    if isinstance(o, datetime.datetime):
        return o.__str__()

@app.route('/')
def hello_world():
    return 'Hello, World!'


@app.route('/temperature')
def query():
    result_array = []
    result = postgresInstance.query("SELECT * FROM temperature")
    for row in result:
        data = {}
        data['mac_id'] = ''.join(row[0])
        data['temperature'] = row[1]
        data['timestamp'] = row[2]
        json_data = json.dumps(data , default = myconverter)
        result_array.append(json_data)

    return jsonify(result_array)

@app.route('/temperatureByID', methods=['GET'])
def queryByID():
    mac_id = request.args.get('mac_id')
    print (mac_id)
    result = postgresInstance.queryByID("select * from temperature where mac_id={} order by timestamp desc",mac_id)
    # print (len(result))
    if(len(result) != 0):
        data = {}
        data['mac_id'] = ''.join(result[0][0])
        data['temperature'] = result[0][1]
        data['timestamp'] = result[0][2]
        json_data = json.dumps(data , default = myconverter)
        return jsonify(json_data)
    else:
        return jsonify({})
        

if __name__ == '__main__':
    connectDB()
    app.run(host='0.0.0.0', port=4000)