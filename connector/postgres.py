import psycopg2

conn = ''
class PostgresDB:
    def connect(self):
        global conn
        conn = psycopg2.connect("dbname='iot_sensor_db' user='robocet' host='18.219.220.211' password='robocet123'")
        if(conn):
            print ("Postgres Connected")
            return conn

    def query(self,queryString):
        cur = conn.cursor()
        cur.execute(queryString)
        rows = cur.fetchall()
        conn.commit()
        cur.close()
        return rows

    def queryByID(self,queryString,mac_id):    
        cur = conn.cursor()
        cur.execute(queryString.format("'"+mac_id+"'"))
        rows = cur.fetchall()
        conn.commit()
        cur.close()
        return rows